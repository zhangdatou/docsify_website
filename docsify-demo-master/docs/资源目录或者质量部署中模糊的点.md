# 资源目录或者质量部署中模糊的点



## 1.关于upload等路径

1.1已知当前路径为空

![image-20200310132749474](C:\Users\17516\AppData\Roaming\Typora\typora-user-images\image-20200310132749474.png)

需要创建路径

但是一堆${}这样的

这里比如

```
file:
  #cdmp操作目录 同时修改 上面server.tomcat.basedir
  basePath: ${server.tomcat.basedir}
```



那basePath的值是什么

配置文件中向上看有这个,server下面的tomcat下面的basedir

```
server:
  port: 8073
  # 不要修改这个发布地址
  context-path: /resource
#cdmp 使用主目录
  tomcat:
    basedir: /home/data-manage/
```

可知${server.tomcat.basedir} = /home/data-manage/

已知途中的路径为

![image-20200310133217370](C:\Users\17516\AppData\Roaming\Typora\typora-user-images\image-20200310133217370.png)



```
${file.basePath}files/upload/
```



去看file下面的bashPath的值是

![image-20200310133228020](C:\Users\17516\AppData\Roaming\Typora\typora-user-images\image-20200310133228020.png)



```
${server.tomcat.basedir}
```



server下面的tomcat下面的basedir是

![image-20200310133238651](C:\Users\17516\AppData\Roaming\Typora\typora-user-images\image-20200310133238651.png)



```
/home/data-manage/
```

可知

${file.basePath}files/upload/ = ${server.tomcat.basedir}files/upload/  = /home/data-manage/files/upload/ 





则实际的上传文件夹路径为

 /home/data-manage/files/upload/ 



尝试去cd /home/data-manage/files/upload/ 

如果失败,一步一步到子级文件夹去mkdir文件夹名称即可

