# 	创建当前jar包的启动脚本







首先在当前文件夹输入命令

~~~linux
touch start.sh
~~~

然后 vi start.sh或者vim start.sh

按i粘贴如下内容

例如:

~~~linux
kill -9 `ps -ef | grep zazt-quality-service.jar | grep -v grep |awk '{print $2}'`
nohup java -jar zazt-quality-service.jar > dev.log 2>&1 &

~~~

写法

~~~linux
kill -9 `ps -ef | grep jar包名 | grep -v grep |awk '{print $2}'`
nohup java -jar jar包名 > dev.log 2>&1 &
~~~

按esc   并:wq

之后启动脚本就创建成功

sh start.sh即可